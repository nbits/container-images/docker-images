Puppet Server
=============

Inspired by [puppet/puppetserver][1], this image runs a Puppet Server with
modules directory pointing to `${CI_PROJECT_DIR}` and an empty default node
definition.

[1]: https://hub.docker.com/r/puppet/puppetserver 
